<?php snippet('header') ?>

<div id="content">
  <div id="googleMap">
    <div id="googcontainer"></div>
  </div>
  <div class="contactContent">
    <div class="contactAddress">
      <div class="blockCentered">
        <div class="scaleit">
        <h2><address><a href="<?php echo $page->googlemaps() ?>" target="_blank"><?php echo $page->street() ?><br/><?php echo $page->citystate() ?></a></address></h2>
        </div>
      </div>
    </div>
    <div class="contactPhone">
      <div class="blockCentered">
        <div class="scaleit">
        <h2><a href="tel:<?php echo $page->telephone() ?>"><?php echo $page->telephone() ?></a><br/>
        <a href="mailto:<?php echo $page->email() ?>"><?php echo $page->email() ?></a></h2>
        </div>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>