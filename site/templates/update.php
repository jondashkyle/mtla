<?php snippet('header') ?>
<?php
  $updatesTags = tagcloud($pages->find('updates'));
?>

  <div id="content">

    <div class="updatesTags">
      <?php foreach($updatesTags as $updatesTag): ?>
        <a href="<?php echo $updatesTag->url() ?>" <?php echo($updatesTag->isActive()) ? 'class="active"' : ''?> data-pjax>
          <span><?php echo $updatesTag->name() ?></span>
        </a>
      <?php endforeach ?>
    </div>

    <div class="updatesContainer">
      <div class="update">
        <h2><?php echo $page->title()?></h2>
        <div class="tags">
        <?php foreach(str::split($page->tags()) as $tag): ?>
          <span><a href="<?php echo url('/updates/tag:' . urlencode($tag)) ?>" data-pjax>
              <?php echo html($tag) ?></a></span>
        <?php endforeach ?>
        </div>
        <div class="updateContent">
          <?php echo kirbytext($page->text())?>
        </div>
      </div>
    </div>

  </div>

<?php snippet('footer') ?>