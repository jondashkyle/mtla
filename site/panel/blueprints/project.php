<?php if(!defined('KIRBY')) exit ?>

title: Project
pages: false
files: true
fields:
  title: 
    label: Title
    type:  text
  tags: 
    label: Tags
    type: tags
    lower: true
  year:
    label: Date
    type: date
  city:
    label: City
    type: text
  state:
    label: State
    type: text
  text: 
    label: Text
    type:  textarea
    size:  large
    buttons: true
  partners: 
    label: Partners
    type:  textarea
    buttons: true
    size:  small
  featured: 
    label: Featured in the slideshow on the homepage?
    type:    checkbox
    default: off