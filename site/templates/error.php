<?php snippet('header') ?>

<div id="content">
  <div id="error">
    <h2><?php echo html($page->title()) ?></h2>
    <?php echo kirbytext($page->text()) ?>
  </div>
</div>

<?php snippet('footer') ?>