# Active

- Hide partners title if there is nothing in the field @done
- Logo TM @done
- Static comp of news page
	- Should be vertically scrolling feed.
	- Automatically make gallery style thing based off how many images are in there.
	- Maybe override that if the entry has a certain tag?
- Animatic of intro reversed animation
	- Might just be easier to make an example of this which is the real thing in browser rather than AE.

# Soon

- Screenshots for paul when site is populated

# Accessibility
http://ec2-54-189-118-1.us-west-2.compute.amazonaws.com