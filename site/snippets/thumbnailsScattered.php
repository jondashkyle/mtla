<div class="thumbnailsScattered">
  <?php $projects = $pages->find('work'); ?>
  <?php foreach($projects->children()->visible()->shuffle() as $project): ?>
    <?php if ($project->hasImages() ): ?>
      <?php 


        $image = $project->images()->first();

        if ( $image->height() > $image->width() ) {
           $size  = mt_rand(1, mt_rand(1, 2));
        } else {
           $size  = mt_rand(1, mt_rand(1, 3));
        }

        if ( $size > 1 ) {

          if ( $image->height() > $image->width() ) {
            $sizeX = $size;
            $sizeY = $size + 1;
          } else if ( $image->width() > $image->height() ) {
            $sizeX = $size + 1;
            $sizeY = $size;
          } else {
            $sizeX = $size;
            $sizeY = $size;
          }

          $sizeXImg = 800;
          $sizeYImg = 800;

        } else {
          $sizeX = $size;
          $sizeY = $size;
          $sizeXImg = 400;
          $sizeYImg = 400;
        }

      ?>
      <div class="thumb size<?php echo $sizeX ?><?php echo $sizeY ?>" data-title="<?php echo $project->title() ?>">
        <?php
          $thumb = thumb($image, array('width' => $sizeXImg , 'height' => $sizeYImg), false); 
          $grey = thumb($image, array('width' => $sizeXImg , 'height' => $sizeYImg, 'grayscale' => true), false); 
        ?>
        <a href="<?php echo $project->url() ?>" data-pjax></a>
        <div 
          class="image color"
          data-style="background-image: url(<?php echo $thumb ?>)"
        ></div>
        <div 
          class="image grey"
          data-style="background-image: url(<?php echo $grey ?>)"
        ></div>
      </div>
    <?php endif ?>
  <?php endforeach ?>
</div>