<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Navigation
pages: false
files: false
fields:
  title: 
    label: Title
    type:  text

  types: 
    label: Types
    lower: true
    type: tags
    index: all
    field: tags

  collections: 
    label: Collections
    lower: true
    type: tags
    index: all
    field: tags