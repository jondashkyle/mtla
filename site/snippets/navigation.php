<?php
  $work = $pages->find('work');
  $tags = tagcloud($work, array(
    'sort'    => 'name',
    'sortdir' => 'asc'
  ));
  $types = $pages->find('navigation')->types();
  $newTags = $pages->find('navigation')->collections();
?>

<div id="filterContainer">
  <div class="filterClose" data-filterHide>&times;</div>

  <div class="filterTitle">Types</div>
  <div class="filterTypes">
    <?php foreach(str::split($types) as $type): ?>
      <a href="<?php echo url('/work/tag:' . urlencode(ucfirst($type))) ?>" data-pjax><span><?php echo $type ?></span></a>
    <?php endforeach ?>
  </div>

  <div class="filterTitle collectionsTitle">Collections</div>
  <div class="filterCollections">
    <?php foreach(str::split($newTags) as $tag): ?>
      <a href="<?php echo url('/work/tag:' . urlencode(ucfirst($tag))) ?>" data-pjax><span><?php echo $tag ?></span></a>
    <?php endforeach ?>
  </div>
</div>

<div id="filterScreen" data-filterHide></div>

<div id="navigation">
  <div class="searchResults"></div>
  <div class="links">
    <input class="searchTerm" placeholder="Search..." />
    <div class="filter">
      <a href="#">Filter work</a>
    </div>
    <?php foreach($pages->visible() as $page): ?>
      <a href="<?php echo $page->url() ?>" data-pjax data-uri="<?php echo $page->uri() ?>"><?php echo $page->title() ?></a>
    <?php endforeach ?>
  </div>
</div>