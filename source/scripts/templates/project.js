/**
 * Project
 */
Site.Project = function() {

  var self = {

    $el : { },

    data : {
      'mousewheelThrottle' : null,
      'slideshowSwipe'     : true,
      'contentSwipe'       : true,
      'contentOpen'        : false
    },

    slideshow : {

      setup : function() {
        self.$el.slideshow
          .slick({
            // 'fade'   : true,
            'useCSS' : true,
            'arrows' : true,
            'dots'   : true,
            'easing' : 'easeInOut',
            'speed'  : 500,
            'lazyLoad' : 'ondemand',
            'accessibility':  true,
            onReInit : self.slideshow.resize,
            onInit   : self.slideshow.resize
          });
      },

      resize : function() {

        var height = $(window).height() - 67;
        var width  = $(window).width();

        self.$el.slideshow.css({
          'height' : height + 'px',
          'width'  : width + 'px'
        });

        self.$el.slideshow.find('.slide').css({
          'height' : height + 'px'
        });

      },

      next : function() {
        self.$el.slideshow.slickNext();
        self.content.hide();
      },

      prev : function() {
        self.$el.slideshow.slickPrev();
        self.content.hide();
      },

      swipe : function(direction, delta) {

        // If we've got the momentum
        if ( delta > 20 ) {

          // Progress the slideshow
          if ( self.data.slideshowSwipe ) {
            if ( direction == 'left'  ) self.slideshow.prev();
            if ( direction == 'right' ) self.slideshow.next();
          }

          // Set the data
          clearTimeout(self.data.mousewheelThrottle);
          self.data.slideshowSwipe = false;

          // Throttle it
          self.data.mousewheelThrottle = setTimeout(function() {
            self.data.slideshowSwipe = true;
          }, 50);

        }

      }

    },

    content : {

      show : function() {
        if ( ! self.data.contentOpen ) {
          self.data.contentOpen = true;
          self.$el.content.velocity({
            'opacity' : 1
          }, {
            'duration' : 300,
            'easing'   : 'easeOutCubic',
            'display'  : 'block'
          });
        }
      },

      hide : function() {
        if ( self.data.contentOpen ) {
          self.data.contentOpen = false;
          self.$el.content.velocity({
            'opacity' : 0
          }, {
            'duration' : 300,
            'easing'   : 'easeOutCubic',
            'display'  : 'none'
          });
        }
      },

      toggle : function() {
        if ( self.data.contentOpen ) {
          self.content.hide();
        } else {
          self.content.show();
        }
      },

      swipe : function(direction, delta) {

        // If we've got the momentum
        if ( delta > 100 ) {

          // Directional
          // if ( self.data.contentSwipe ) {
          //   if ( direction == 'up'   ) self.content.hide();
          //   if ( direction == 'down' ) self.content.show();
          // }

          // Bi-directional
          if ( self.data.contentSwipe ) self.content.toggle();

          // Set the data
          clearTimeout(self.data.mousewheelThrottle);
          self.data.contentSwipe = false;

          // Throttle it
          self.data.mousewheelThrottle = setTimeout(function() {
            self.data.contentSwipe = true;
          }, 150);

        }

      },

      resize : function() {
        self.$el.content.css({
          'height' : $(window).height() - 67
        })
      },

      contentHide : function(event) {
        if ( event.toElement === event.currentTarget ) self.content.hide();
      }

    },

    keynavigation : function(event) {
      var key = event.which;
      if ( key === 37 ) {
        self.slideshow.prev();
      } else if ( key === 39 ) {
        self.slideshow.next();
      } else if ( key === 38 || key === 40 ) {
        self.content.toggle();
      } else if ( key === 27 ) {
        self.content.hide();
      }
    },

    mousewheel : function(event) {

      // Up and Down
      var deltaY = event.deltaY;
      var dirY = deltaY > 0 ? 'up' : 'down';
      var velY = Math.abs(deltaY);

      // Left and Right
      var deltaX = event.deltaX;
      var dirX = deltaX > 0 ? 'right' : 'left';
      var velX = Math.abs(deltaX);

      // Fire it
      // if ( velY > 0 ) self.content.swipe(dirY, velY);
      if ( velX > 0 ) {
        self.slideshow.swipe(dirX, velX);
        event.preventDefault();
      }

      
    },

    show : function() {

      // Fade in the dots
      self.$el.slideshow
        .find('.slick-dots li')
        .velocity('transition.expandIn', {
          'stagger'  : 100,
          'duration' : 1000,
          'display'  : null
        });

      $('.informationProject').velocity({
        'opacity': 1
      }, {
        'delay' : 500,
        'duration' : 500,
        'easing' : 'easeOut'
      });

      $('.slick-prev')
        .velocity({ 'translateX' : 100 }, 0)
        .velocity({
          'translateX' : 0,
          'opacity'    : 1
        }, {
          'easing'   : 'easeOutCubic',
          'duration' : 500,
          'delay'    : 500
        });

      $('.slick-next')
        .velocity({ 'translateX' : -100 }, 0)
        .velocity({
          'translateX' : 0,
          'opacity'    : 1
        }, {
          'easing'   : 'easeOutCubic',
          'duration' : 500,
          'delay'    : 500
        });

    },

    resize : function() {
      self.slideshow.resize();
      self.content.resize();
    },

    load : function() {

      // Cache
      self.$el.slideshow = $('#slideshowProject');
      self.$el.content   = $('#contentProject');

      // Window events
      $(window) 
        .on('keydown', self.keynavigation)
        .on('resize', self.resize).trigger('resize')
        .on('mousewheel', self.mousewheel)
        .scrollTop(0);

      // Info click
      $('.informationProject').on('click', self.content.show);
      self.$el.content.on('click', self.content.contentHide);

      // Project events
      self.$el.content.on('click', '[data-contentClose]', self.content.hide);

      self.slideshow.setup();
      self.show();
      self.content.hide();

    },

    unload : function() {
      $(window)
        .off('keydown', self.keynavigation)
        .off('mousewheel', self.mousewheel)
        .off('scroll', self.scroll)
        .off('resize', self.resize);
    }

  };

  return {
    load   : self.load,
    unload : self.unload
  };

}();