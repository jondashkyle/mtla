<?php if(!defined('KIRBY')) exit ?>

title: Work
pages:
  template: project
  limit: 30
files: true
fields:
  title: 
    label: Title
    type:  text