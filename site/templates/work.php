<?php snippet('header') ?>
<?php
  if(param('tag')) {
    $projects = $page->children()->visible()->filterBy('tags', strtolower(urldecode(param('tag'))), ',')->shuffle();
  } else {
    $projects = $page->children()->visible()->shuffle();
  }
?>

<div id="content">

  <?php if(param('tag')): ?>
    <div class="workFilter">
      <a href="<?php echo url('/work/tag:' . urlencode(param('tag'))) ?>" data-pjax><?php echo urldecode(param('tag')) ?></a>
    </div>
  <?php endif ?>

  <div id="contentWork">
    <?php foreach ( $projects as $project ): ?>
    <div class="thumbWork loading">
      <?php $image = $project->images()->first() ?>
      <a href="<?php echo $project->url() ?>" data-pjax class="title">
        <div class="thumbImg">
          <?php if($image): ?>
          <img src="<?php echo thumb($image, array('width' => 600 , 'height' => 600), false); ?>" />
          <?php endif ?>
        </div>
        <?php echo $project->title() ?>
      </a>
      <div class="tags">
      <?php foreach(str::split($project->tags()) as $tag): ?>
        <span><a href="<?php echo url('/work/tag:' . urlencode($tag)) ?>" data-pjax><?php echo html($tag) ?></a></span>
      <?php endforeach ?>
      </div>
    </div>
    <?php endforeach ?>
  </div>

</div>

<?php snippet('footer') ?>