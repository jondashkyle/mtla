<?php if(!defined('KIRBY')) exit ?>

title: Individual
pages: false
files: true
fields:
  title: 
    label: Name
    type:  text
  email: 
    label: Email
    type:  text
    validate: email
  biography:
    label: Biography
    type:  textarea
    size:  large