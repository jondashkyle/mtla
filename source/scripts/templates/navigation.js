/**
 * Navigation
 */
Site.Navigation = function() {

  var self = {

    data : {
      'filterOpen'   : false
    },

    Logo : {

      show : function() {
        // Show the logo
        $('#logo').velocity({
          'opacity'    : 1
        }, {
          'duration' : 750,
          'easing'   : 'easeInOutCubic',
          'delay'    : 500
        });
      }

    },

    Filter :{

      hide : function(event) {

        // Update global data
        self.data.filterOpen = false;

        // Reset scroll
        var resetScroll = false;
        if ( event !== undefined ) resetScroll = event.data.resetScroll;

        $('.filter a').removeClass('active');

        $('#filterScreen').velocity({
          'opacity' : 0
        }, {
          'duration' : 600,
          'display'  : 'none',
          'easing'   : 'easeInOutQuad'
        });

        $('#filterContainer').velocity({
            'translateY' : '100%'
          }, {
            'duration' : 500,
            'easing'   : 'easeInOutExpo',
            'display'  : 'none'
          });

        $('#navigation').velocity({
            'translateY' : 0
          }, {
            'duration' : 500,
            'easing'   : 'easeInOutExpo'
          });

        if ( resetScroll ) {
          $('html').velocity('scroll', {
            'duration' : 500,
            'easing'   : 'easeInOutExpo',
            'offset'   : $(window).scrollTop() - $('#filterContainer').outerHeight()
          });
        }

      },

      show : function() {

        self.data.filterOpen = true;

        $('.filter a').addClass('active');

        $('#filterScreen').velocity({
          'opacity' : 1
        }, {
          'duration' : 600,
          'display'  : 'block',
          'easing'   : 'easeInOutQuad'
        });

        $('#filterContainer').velocity({
            'translateY' : '0%'
          }, {
            'duration' : 500,
            'easing'   : 'easeInOutExpo',
            'display'  : 'block'
          });

        $('#navigation').velocity({
            'translateY' : $('#filterContainer').outerHeight() * -1
          }, {
            'duration' : 500,
            'easing'   : 'easeInOutExpo'
          });

        $('html').velocity('scroll', {
          'duration' : 500,
          'easing'   : 'easeInOutExpo',
          'offset'   : $(window).scrollTop() + $('#filterContainer').outerHeight()
        });

        // Hide on scroll
        $(window).one('mousewheel', { 
          resetScroll : false
        }, self.Filter.hide);

      },

      toggle : function(event) {
        if ( self.data.filterOpen ) {
          self.Filter.hide({
            data : {
              resetScroll : true
            }
          });
        } else {
          self.Filter.show();
        }
        
        event.preventDefault();
      }

    },

    setActive : function(href) {

      $('#navigation a').removeClass('active');

      $('#navigation')
        .find('a[data-uri="' + href + '"]')
        .addClass('active');

    },

    show : function() {
      $('#navigation').velocity({
        'translateY' : '0%'
      }, {
        'duration' : 500,
        'easing'   : 'easeOutCubic',
        'display'  : 'block'
      });
    },

    resize : function() {

      var width  = $('#filterContainer').width();
      var colWidth = 100 / Math.floor(width / 215);

      vein.inject('#filterContainer .filterCollections a', {
        'width' : colWidth + '%'
      });

      if ( self.data.filterOpen ) {
        $('#navigation').velocity({
          'translateY' : $('#filterContainer').outerHeight() * -1
        }, 0);
      }

    },

    setup : function() {

      // Position filter
      $('#filterContainer').velocity({
          'translateY' : '100%'
        }, {
          'duration' : 0
      });

      // Events
      $('.filter a').on('click', self.Filter.toggle);
      $('body').on('click', '[data-filterHide]', { 
        resetScroll : true
      }, self.Filter.hide);

      $(window).on('resize', self.resize).trigger('resize');

    }

  };

  // Initialize
  $(self.setup);

  // Public
  return {
    show       : self.show,
    filterShow : self.Filter.show,
    filterHide : self.Filter.hide,
    logoShow   : self.Logo.show,
    setActive  : self.setActive
  }

}();