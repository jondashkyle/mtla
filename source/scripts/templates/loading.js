/**
 * Loading
 */
Site.Loading = function() {

  var self = {

    $el : {

    },

    show : function() {
      self.$el.container.velocity({
        'opacity'  : 1,
        'scale'    : 1
      }, {
        'duration' : 200,
        'easing'   : 'easeOutQuint',
        'display'  : 'block'
      });
      self.$el.spinner.show();
    },

    hide : function() {
      self.$el.container.velocity({
        'opacity'  : 0,
        'scale'    : 0.5
      }, {
        'duration' : 400,
        'easing'   : 'easeInQuint',
        'display'  : 'none',
        complete   : function() {
          self.$el.spinner.hide();
        }
      });
    },

    setup : function() {

      // Cache
      self.$el.container = $('.loadingContainer');
      self.$el.spinner   = $('.loadingSpinner');

    }

  };

  // Init
  $(self.setup);

  return {
    show : self.show,
    hide : self.hide
  };

}();