<?php if(!defined('KIRBY')) exit ?>

title: Update
pages: false
files: true
fields:
  title: 
    label: Title
    type:  text
  tags: 
    label: Tags
    type: tags
    lower: true
  date:
    label: Date
    type: date
  text: 
    label: Text
    type:  textarea
    size:  large
    buttons: true