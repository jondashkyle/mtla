<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Press logos
pages:
  template: presslogo
  limit: 30
files: true
fields:
  title: 
    label: Title
    type:  text