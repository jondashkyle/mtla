/**
 * Contact
 */

Site.Contact = function() {

  var self = {

    map    : null,
    latlng : 0,

    mapMake : function() {

      self.latlng = new google.maps.LatLng(34.016108, -118.494712);

      var myOptions = {
          zoom: 15,
          disableDefaultUI: true,
          center: self.latlng,
          draggable : false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 65
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#34CFA1"
                    }
                ]
            },
            // {
            //     "featureType": "administrative",
            //     "elementType": "labels.text.fill",
            //     "stylers": [
            //         {
            //             "color": "#34CFA1"
            //         }
            //     ]
            // },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 51
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 30
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 40
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffff00"
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            }
        ]
      };

      self.map = new google.maps.Map(document.getElementById('googcontainer'), myOptions);
      var image = new google.maps.MarkerImage('static/images/mtlaPin.png', null, null, null, new google.maps.Size(30,48)); 

      // To add the marker to the map, use the 'map' property
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(34.01608, -118.49471),
        // title: "MTLA",
        icon : image,
        url  : 'https://www.google.com/maps/place/1424+4th+St+%23234/@34.0158323,-118.4946633,17z/data=!3m1!4b1!4m2!3m1!1s0x80c2a4ce2e5afc99:0xf5f531fd2f704143'
      });

      // To add the marker to the map, call setMap();
      marker.setMap(self.map);

      // Open it up in google maps
      google.maps.event.addListener(marker, 'click', function() {
        window.open(marker.url);
      });

      $(window).on('mousemove', function(event) {
        var x = (event.clientX - $(window).width() / 2);
        var y = ((event.clientY - $(window).height() / 2) * -1);
        self.mapOffset(self.latlng, x / 8, y / 8);
      });

    },

    mapOffset : function offsetCenter(latlng,offsetx,offsety) {

      var scale = Math.pow(2, self.map.getZoom());
      var nw = new google.maps.LatLng(
          self.map.getBounds().getNorthEast().lat(),
          self.map.getBounds().getSouthWest().lng()
      );

      var worldCoordinateCenter = self.map.getProjection().fromLatLngToPoint(latlng);
      var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0)

      var worldCoordinateNewCenter = new google.maps.Point(
          worldCoordinateCenter.x - pixelOffset.x,
          worldCoordinateCenter.y + pixelOffset.y
      );

      var newCenter = self.map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

      self.map.setCenter(newCenter);

    },

    transition : function() {

      $('.contactContent .scaleit').velocity({
        'scale' : 1.2,
        'opacity' : 0,
        'translateZ' : 1
      }, {
        'duration' : 0
      });

      $('.contactAddress .scaleit').velocity({
        'scale' : 1,
        'opacity' : 1
      }, {
        'duration' : 500,
        'delay'    : 250,
        'easing'   : 'easeOutQuad'
      });

      $('.contactPhone .scaleit').velocity({
        'scale' : 1,
        'opacity' : 1
      }, {
        'duration' : 500,
        'easing'   : 'easeOutQuad',
        'delay'    : 500
      });

      $('#googleMap #googcontainer').velocity({
        'opacity' : 1,
      }, {
        'duration' : 3000,
        'easing'   : 'easeOutQuad',
        'delay'    : 500
      });

    },

    load : function() {
      self.transition();
      setTimeout(self.mapMake ,10);
    },

    unload : function() {

    }

  };

  // Maps
  // google.maps.event.addDomListener(document, 'mapLoad', self.map);

  return {
    load   : self.load,
    unload : self.unload
  };

}();