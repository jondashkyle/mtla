<?php snippet('header') ?>
<?php

  $updatesTags = tagcloud($pages->find('updates'));

  if(param('tag')) {
    $updates = $page->children()->visible()->filterBy('tags', strtolower(urldecode(param('tag'))), ',');
  } else {
    $updates = $page->children()->visible()->shuffle();
  }

?>

  <div id="content">

    <div class="updatesTags">
      <?php foreach($updatesTags as $updatesTag): ?>
        <a href="<?php echo $updatesTag->url() ?>" <?php echo ($updatesTag->isActive()) ? 'class="active"' : ''?> data-pjax>
          <span><?php echo $updatesTag->name() ?></span>
        </a>
      <?php endforeach ?>
    </div>

    <div class="updatesContainer">
      <?php foreach($updates as $p ): ?>
      <div class="update">
        <h2>
          <a href="<?php echo $p->url() ?>">
            <?php echo $p->title()?>
          </a>
        </h2>
        <div class="tags">
        <?php foreach(str::split($p->tags()) as $tag): ?>
          <span><a href="<?php echo url('/updates/tag:' . urlencode($tag)) ?>" data-pjax>
              <?php echo html($tag) ?></a></span>
        <?php endforeach ?>
        </div>
        <div class="updateContent">
          <?php echo kirbytext($p->text())?>
        </div>
      </div>
      <?php endforeach ?>
    </div>

  </div>

<?php snippet('footer') ?>