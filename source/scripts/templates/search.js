/**
 * Search
 */
Site.Search = function() {

  var self = {

    $el : { },

    'data' : {
      'throttle' : null,
      'active' : false,
      'projects' : [ ],
    },

    show : function() {
      clearTimeout(self.data.throttle);
      if ( self.data.active ) return false;
      self.$el.results
        .velocity('transition.slideUpIn', 250);
      self.data.active = true;
    },

    hide : function() {
      clearTimeout(self.data.throttle);
      if ( ! self.data.active ) return false;
      self.data.throttle = setTimeout(function() {
        self.$el.results.velocity('transition.slideDownOut', 250);
        self.data.active = false;
      }, 1000);
    },

    focus : function() {
      if ( self.$el.results.find('a, span').length > 0 ) {
        self.show();
      }
    },

    update : function(event) {

      var term    = self.$el.input.val();
      var search  = [ ];
      var filters = [ ];
      var results = '';

      var sort = function(a, b) {
        var aName = a.score;
        var bName = b.score; 
        return ((aName > bName) ? -1 : ((aName < bName) ? 1 : 0));
      };

      // Search it
      if ( term !== '' ) {

        if ( ! self.data.active ) self.show();

        $.each(self.data.projects, function(i, project) {
          var score = LiquidMetal.score(project.title + ' ' + project.tags, term);
          project.score = score;
          if ( score > 0.5 && search.length < 6 ) {
            search.push(project);
          }
        });

        // Sort it
        search.sort(sort);

        // Format it
        $.each(search, function(i, project) {
          results += '<a href="' + project.url + '" class="result" data-pjax data=score="' + project.score + '"><div class="thumb" style="background-image: url(' + project.thumb + ')"></div><span class="title">' + project.title + '</span><span class="tags">' + project.tags + '</span></div>'; 
        });

        // No results
        if ( results == '' ) {

          var tags = '';

          // Grab the tags
          $('#filterContainer a').each(function() {
            filters.push($(this).text());
          });

          // Pick a few and show'em
          for (var i = 4; i >= 0; i--) {
            var filter = filters[Math.floor(Math.random() * filters.length)];
            filters.splice($.inArray(filter, filters),1);
            tags += '<span class="tag"><span class="tagtext">' + filter + '</span></span>';
          };

          results = '<div class="noresults">Suggestions:<br/>' + tags + '</div>';

        }

        // Show it
        self.$el.results.html(results);

      } else {

        if ( self.data.active ) {
          self.hide();
          setTimeout(function() {
            self.$el.results.html(results);
          }, 500);
        }

      }

    },

    tag : function(event) {
      var $tag = $(event.currentTarget);
      self.$el.input
        .val($tag.text())
        .trigger('keyup')
        .focus();
      clearTimeout(self.data.throttle);
      setTimeout(self.show, 100);
    },

    populate : function() {
      // Request the projects and store
      $.getJSON('/work/search', function(data) {
        self.data.projects = data;         
      });
    },

    setup : function() {

      // Cache
      self.$el.input   = $('.searchTerm');
      self.$el.results = $('.searchResults');

      $(document).on('pjax:complete', self.hide);

      // Events
      $('.searchTerm')
        .one('focus', self.populate)
        .on('keyup', self.update)
        .on('focus', self.focus)
        .on('blur', self.hide);

      $('.searchResults')
        .on('mouseenter', self.show)
        .on('mouseleave', self.hide)
        .on('click', '.tagtext', self.tag);

    }

  };

  // Init
  $(self.setup);

  return {

  }

}();