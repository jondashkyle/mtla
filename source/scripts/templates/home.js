Site.Home = function() {

  var self = {

    $el : { },

    data : {
      'positionThrottle' : null
    },

    options : {

      thumbnailPositions : [
        'left top', 'center top', 'right top', 
        'left center', 'center', 'right center', 
        'left bottom', 'center bottom', 'right bottom'
      ]

    },

    slideshow : {

      setup : function() {
        self.$el.slideshow
          .slick({
            'arrows' : true,
            'dots'   : true,
            'easing' : 'easeInOut',
            'speed'  : 500,
            'autoplay' : false,
            'autoplaySpeed' : 4 *1000,
            onBeforeChange : function() {
              $('.titleContainer').velocity({
                'opacity' : 0
              }, {
                'duration' : 250
              });
            },
            onAfterChange : self.slideshow.titleUpdate,
            onReInit : self.slideshow.resize,
            onInit   : self.slideshow.resize
          });
      },

      titleUpdate : function() {
        var $el = $('.slick-slide.slick-active').find('.img');
        var title = $el.attr('data-title');
        var location = $el.attr('data-location');
        var url = $el.attr('data-url');
        $('.slideshowHomeTitle a').text(title).attr('href', url);
        $('.slideshowHomeTitle .location').text(location);
        $('.titleContainer').velocity({
          'opacity' : 1
        }, {
          'duration' : 250
        });
      },

      titleShow : function() {
        $('.slideshowHomeTitle').velocity({
          'translateY' : 0
        }, {
          'duration' : 500,
          'easing'   : 'easeOutCubic',
        });
      },

      scale : function() {
        return $(window).height() / $(window).width();
      },

      resize : function() {

        var scale = self.slideshow.scale();
        var height = ($(window).height() - 65) * 0.25 / 2;

        $('.slideshowHome').css({
          'width' : $(window).width() + 'px'
        });

        $('.slideshowHome .slide').css({
          'height' : $(window).width() * scale - 65 + 'px'
        });

        $('.titleContainer, .slick-dots').css({
          'height' : height,
          'line-height' : height + 'px'
        })

      },

      showDots : function() {
        // Fade in the dots
        self.$el.slideshow
          .find('.slick-dots li')
          .velocity('transition.expandIn', {
            'stagger'  : 100,
            'duration' : 1000,
            'display'  : null
          });
      }

    },

    thumbnails : {

      position : function() {

        var width  = $(window).width() + 10;
            width = width / Math.floor(width / 150);
            width = 2.0 * Math.round(width/2.0);

        self.$el.thumbnails.nested({
          selector: '.thumb',
          minWidth: width,
          gutter : 0,
          resizeToFit: false,
          animate: false
        });

      },

      format : function(i, element) {

        var $this = $(element);
        var position = self.options.thumbnailPositions[Math.floor(Math.random() * self.options.thumbnailPositions.length)];
        var orphans  = ['<div class="thumb blank size11 white" />', '<div class="thumb blank size11 white" />', '<div class="thumb blank size11 black" />', '<div class="thumb blank size11 grey" />'];
        var colors   = ['grey', 'black','black', 'green', 'nocolor', 'nocolor'];
        var color    = colors[Math.floor(Math.random() * colors.length)];
        var chance   = Math.floor(Math.random() * 2); 

        $(this).addClass(color);

        // Blank guy
        if ( chance == 0 ) {
          var orphan = orphans[Math.floor(Math.random() * orphans.length)];
          $(this).after(orphan);
        }

      },

      load : function() {
        $('.work .thumb').each(function() {

          // Setup the image bg
          $(this).find('.image').each(function() {
            $(this).attr('style', $(this).attr('data-style'));
          });

          // Fade in on load
          $(this).waitForImages({
            waitForAll: true,
            finished: function() {
              $(this).addClass('loaded');
            }
          });

        });
      },

      titleShow : function(event) {
        var $thumb = $(event.currentTarget);

        // If we're scrolled into position
        if ( $(window).scrollTop() > $('.work').position().top - ($(window).height() / 2) ) {
          self.$el.homeTitle
            .text($thumb.attr('data-title'))
            .addClass('active');
        }
      },

      titleHide : function(event) {
        var $thumb = $(event.currentTarget);
        self.$el.homeTitle
          .removeClass('active');
      }

    },

    resize : function() {
      self.slideshow.resize();
      clearTimeout(self.data.positionThrottle);
      self.data.positionThrottle = setTimeout(self.thumbnails.position, 250);
    },

    load : function() {

      // Cache
      self.$el.container  = $('.work');
      self.$el.slideshow  = $('.slideshowHome');
      self.$el.homeTitle  = $('.homeTitle');
      self.$el.thumbnails = $('.thumbnailsScattered');

      // Format the thumbnails
      $('.work .thumb').each(self.thumbnails.format);

      // Hover
      $('.work .thumb:not(.blank)').on('mouseenter', self.thumbnails.titleShow);
      $('.work .thumb').on('mouseleave click', self.thumbnails.titleHide);

      // Add a random green one in there
      $('.work .thumb')
        .eq(Math.floor(Math.random() * $('.work .thumb').length))
        .after('<div class="thumb blank size11 green" />');

      // Setup
      self.slideshow.setup();
      self.thumbnails.position();

      // Load the thumbnails
      if ( ! Site.Intro.options.active ) {
        self.thumbnails.load();
        self.slideshow.showDots();
      }

      // Events
      $(window).on('resize', self.resize).trigger('resize');

    },

    unload : function() {
      $(window).off('resize', self.resize);
    }

  };

  // Public
  return {
    load   : self.load,
    unload : self.unload,
    slideshowTitleShow : self.slideshow.titleShow,
    loadThumbnails : self.thumbnails.load,
    showDots       : self.slideshow.showDots
  }

}();