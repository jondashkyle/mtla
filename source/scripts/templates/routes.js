/**
 * Routes
 */
Site.Routes = function() {

  var self = {

    options : {
      'active'    : null,
      'target'    : 'a[data-pjax]',
      'container' : '#content'
    },

    send : function() {
      Site.Loading.show();
      Site.Navigation.filterHide();
    },

    complete : function() {
      Site.Loading.hide();
      $('.pressLightbox').remove();
    },

    setup : function() {

      if ($.support.pjax) {

        // Disable timeout
        $.pjax.defaults.timeout = false;

        if ($.support.pjax) {
          $(document).on('click', 'a[data-pjax]', function(event) {
            $.pjax.click(event, '#content', {
              'fragment' : self.options.container
            });
            event.preventDefault();
          });
        }

      }

      // Events
      $(document).on('pjax:send', self.send);
      $(document).on('pjax:complete', self.complete);

    }

  };

  $(self.setup);

  return {

  }

}();