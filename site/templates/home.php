<?php snippet('header') ?>
<?php snippet('intro') ?>

<?php
  $headerProjects = $pages->find('work')->children()->filterBy('featured', 'on', ',')->shuffle()->limit(5);
?>

<div id="content">

<div class="slideshowHomeTitle">
  <div class="titleContainer"><a href="<?php echo $headerProjects->first()->url() ?>" data-pjax><?php echo $headerProjects->first()->title() ?></a> — <span class="location"><?php echo $headerProjects->first()->city() ?>, <?php echo $headerProjects->first()->state() ?></span></div>
</div>

<div class="slideshowHome">
  <?php foreach($headerProjects as $project): ?>
    <?php
      $image = $project->images()->find('featured.jpg');
      if ( !$image)  $image = $project->images()->find('featured.jpeg');
      if ( !$image ) $image = $project->images()->first();
    ?>
  <div class="slide">
    <div
      class="img" 
      data-title="<?php echo $project->title() ?>"
      data-location="<?php echo $project->city() ?>, <?php echo $project->state() ?>"
      data-url="<?php echo $project->url() ?>"
      style="background-image: url(<?php echo thumb($image, array('width' => 1800 , 'height' => 1800), false) ?>);"
    ><a href="<?php echo $project->url() ?>" data-pjax></a></div>
  </div>
  <?php endforeach ?>

</div>
  
  <div class=" intro">
    <h2><?php echo kirbytext($page->intro()) ?></h2>
  </div>

  <?php if(!browser::mobile()): ?>
    <div class="work" >
      <div class="homeTitle"></div>
      <?php snippet('thumbnailsScattered') ?>
    </div>
  <?php endif ?>

</div>

<?php snippet('footer') ?>