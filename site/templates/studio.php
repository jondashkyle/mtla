<?php snippet('header') ?>
<?php
  $contact     = $pages->find('contact');
  $individuals = $page->children()->visible();
  $instagram   = instagram('1444007868.511b396.81a14fc3f21a4f248b3708acd2c25741', 10);
  $images      = $instagram->images();
?>

<div id="content">

  <div id="studio">
    <h2><?php echo kirbyText($page->text()) ?></h2>
  </div>

  <div class="studioStatement">
    <?php echo kirbyText($page->statement()) ?>
  </div>

  <div class="instagrams">
    <div class="instagramPhotos">
      <?php if($images): ?>
        <?php foreach ($images as $image): ?>
        <div class="instagram-photo">
          <a href="<?php echo $image->link ?>"><img src="<?php echo $image->url ?>" /></a>
        </div>
        <?php endforeach ?>
      <?php endif ?>
    </div>
  </div>

  <div id="cards">

    <?php foreach( $individuals as $individual ): ?>
    <div class="individual">
      <div class="cardContainer">
        <div class="individualName">&nbsp;</div>
        <div class="card">
          <div class="name">
            — <?php echo $individual->title() ?><br/>
            <a href="mailto:<?php echo $individual->email() ?>"><?php echo $individual->email() ?></a>
          </div>
          <div class="address">
            <a href="<?php echo $contact->googlemaps() ?>" target="_blank"><?php echo $contact->street() ?><br/>
            <?php echo $contact->citystate() ?> 90401</a>
          </div>
          <div class="contact">
            <span class="contactLable">T /</span> — <a href="tel:<?php echo $contact->telephone() ?>"><?php echo $page->telephone() ?></a><br/>
            <a href="/" data-pjax>marktessier.com</a>
          </div>
          <?php snippet('logoExpanded') ?>
        </div>
      </div>
      <div class="biography">
        <div class="individualName"><?php echo $individual->title() ?></div>
        <?php echo kirbyText($individual->biography()) ?>
      </div>
    </div>
    <?php endforeach ?>

  </div>

  <div id="credit">
    Site design by <a href="http://hypetype.co.uk" target="_blank">HypeType</a>
  </div>

</div>

<?php snippet('footer') ?>