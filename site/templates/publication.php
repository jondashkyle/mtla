<?php snippet('header') ?>

  <div class="publication-container">

    <div class="publication-images">
      <?php foreach( $page->images() as $image ): ?>
        <div class="publication-image">
          <img src="<?php echo thumb($image, array('width' => 1400 , 'height' => 1400), false); ?>">
        </div>
      <?php endforeach; ?>
    </div>

    <div class="publication-text">
      <h2><strong><?= $page->title ?></strong></h2><br/>
      <?= kirbytext($page->text()) ?>
    </div>

  </div>

<?php snippet('footer') ?>