/**
 * Studio
 */
Site.Studio = function() {

  var self = {

    resize : function() {
      var width  = $('#cards').width();
      var cardWidth = width * 0.5;

      // Max and min
      if ( cardWidth > 600 ) cardWidth = 600;
      if ( width < 737 ) cardWidth = width;

      vein.inject('.cardContainer', {
        'font-size' : cardWidth / 36 + 'px',
        'line-height' : cardWidth / 26 + 'px'
      });
    },

    load : function() {
      $(window).on('resize', self.resize).trigger('resize');
    },

    unload : function() {
      $(window).off('resize', self.resize);
    }

  };

  return {
    load   : self.load,
    unload : self.unload
  };

}();