<?php snippet('header') ?>
<?php

  if(param('tag')) {
    $presses = $page->children()->find('blocks')->children()->visible()->filterBy('tags', strtolower(urldecode(param('tag'))), ',')->shuffle();
  } else {
    $logos   = $page->children()->find('logos')->children()->visible();
    $presses = $page->children()->find('blocks')->children()->visible();
    $presses = Pages::merge($logos, $presses);
  }

?>
<div id="content">

  <div class="pressContainer">
    <div 
      class="pressBox" 
    ><div class="pressBlock"></div></div>
    <?php foreach($presses->shuffle() as $p): ?>
    <?php 
      $link  = $p->link();
      $images = $p->images(); 
      $title = $p->title();
      $publication = $p->publication();
      $tags = $p->tags();
    ?>
    <div 
      class="pressBox" 
      <?php if( $p->link == '' ) echo 'data-solo-uid="' . $p->uid() . '"'; ?>
    >
      <div class="pressBlock">

        <?php if( $p->link() != '' && $p->parent()->title() != 'Logos'): ?>

          <a href="<?php echo $link ?>" class="pressLinkTitle" target="_blank">
            <?php if(!empty($title)): ?>
              <div class="pressTitle"><strong><?php echo $p->title() ?></strong></div>
            <?php endif; ?>
            <?php if(!empty($publication)): ?>
              <div class="pressPub"><?php echo $p->publication() ?></div>
            <?php endif; ?>
          </a>

        <?php else: ?>
          <?php if( $p->parent()->title() != 'Logos'): ?>
            <a href="<?php echo $p->url() ?>">
          <?php elseif( $p->parent()->title() == 'Logos' ): ?>
            
          <?php else: ?>
            <a href="<?php echo $p->link() ?>" target="_blank">
          <?php endif; ?>

          <?php if( $p->hasImages() ): ?>
          <div class="pressThumb">
            <?php foreach($images as $image): ?>
              <img src="<?php echo thumb($image, array('width' => 600 , 'height' => 600), false); ?>">
            <?php endforeach; ?>
          </div>
          <?php endif; ?>

          <?php if(!empty($tags)): ?>
          <div class="pressTags" style="display: none;">
            <?php foreach(str::split($p->tags()) as $tag): ?>
            <span><a href="<?php echo url('/press/tag:' . urlencode($tag)) ?>" data-pjax><?php echo html($tag) ?></a></span>
            <?php endforeach ?>
          </div>
          <?php endif; ?>

          <?php if( $p->parent()->title() != 'Logos'): ?>
            </a>
          <?php endif; ?>

        <?php endif; ?>
      </div>
    </div>
    <?php endforeach ?>
  </div>

</div>
<?php snippet('footer') ?>