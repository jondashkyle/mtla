/**
 * Work
 */
Site.Work = function() {

  var self = {

    'data' : {
      'columnWidth' : 0,
    },

    'options' : {
      'columnBreakWidth' : 300,
      'columnMaxWidth' : 600
    },

    resize : function(event) {

      var width = $('#contentWork').width();
      self.data.columnWidth = width / Math.floor(width / self.options.columnBreakWidth);

      vein.inject('#contentWork .thumbWork', {
        'width' : self.data.columnWidth + 'px'
      });

    },

    load : function() {

      // Load images
      $('.thumbWork').each(function() {
        var $thumb = $(this);
        var $image = $(this).find('img');
        $(this).imagesLoaded(function() {
          // Fade in if above viewport otherwise just show
          if ( $image.position().top < $(window).height() ) {
            $image.velocity({
              'opacity' : 1
            },{
              'display' : 'block',
              complete  : function() {
                $thumb.removeClass('loading');
              }
            });
          } else {
            $image.css({
              'opacity' : 1
            });
            $thumb.removeClass('loading');
          }
        });
      });


      // Events
      $(window).on('resize', self.resize).trigger('resize');

    },

    unload : function() {
      $(window).off('resize', self.resize);
    }

  };

  return {
    load   : self.load,
    unload : self.unload
  };

}();