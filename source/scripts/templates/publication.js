/**
 * Publication
 */
Site.Publication = function() {

  var self = {

    slideshow : {

      $el : null,

      init : function() {
        self.slideshow.$el = $('.publication-images');
        self.slideshow.$el.images = self.slideshow.$el.find('> .publication-image');
        if ( self.slideshow.$el.images.length > 1 ) {
          self.slideshow.$el.images.on('click', self.slideshow.next);
        }
      },

      next : function() {
        $(this).appendTo(self.slideshow.$el);
      }

    },

    resize : function() {
    
    },

    load : function() {
      self.slideshow.init();
      $(window).on('resize', self.resize).trigger('resize');
    },

    unload : function() {
      $(window).off('resize', self.resize);
    }

  };

  return {
    load   : self.load,
    unload : self.unload
  };

}();