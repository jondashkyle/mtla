var gulp         = require('gulp')
  , concat       = require('gulp-concat')
  , watch        = require('gulp-watch')
  , uglify       = require('gulp-uglify')
  , minifyCSS    = require('gulp-minify-css')
  , sass         = require('gulp-sass')
  , livereload   = require('gulp-livereload')
  , plumber      = require('gulp-plumber')
  , gutil        = require('gulp-util')
  , autoprefixer = require('gulp-autoprefixer')
  , server;


/**
 * Paths
 */

var paths = {

  styles : {
    base : './source/styles',
    dest : './assets'
  },

  scripts : {
    base : './source/scripts',
    dest : './assets',
  }

};
 
 
/**
 * CONCAT
 */
 
var css = [
  paths.styles.base + '/mixins.scss',
  paths.styles.base + '/index.scss',
  paths.styles.base + '/*.scss',
];

var libraries = [
  paths.scripts.base + '/libraries/jquery.js',
  paths.scripts.base + '/libraries/velocity.js',
  paths.scripts.base + '/libraries/velocity.ui.js',
  paths.scripts.base + '/libraries/*.js',
];
 
var js = [
  paths.scripts.base + '/index.js',
  paths.scripts.base + '/templates/grid.js',
  paths.scripts.base + '/templates/loading.js',
  paths.scripts.base + '/templates/*.js',
];
 
 
/**
 * Error handling
 */
 
var onError = function (err) {  
  gutil.beep();
  gutil.log(gutil.colors.white(err.plugin), gutil.colors.red(err.message));
};
 
 
/**
 * CSS
 */
 
gulp.task('styles', function() {
 
  gulp.src(css)
    .pipe(concat("index.css"))
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass())
    .pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
    .pipe(minifyCSS())
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(livereload());
 
});

/**
 * Skrollr
 */
 
gulp.task('skrollr', function() {
  gulp.src(paths.styles.base + '/skrollr.css')
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(livereload());
});
 
/**
 * JS
 */
 
gulp.task('scripts', function() {
 
  // Libraries
  gulp.src(libraries)
    .pipe(concat('libraries.js'))
    .pipe(gulp.dest(paths.scripts.dest));

 // Feed
  gulp.src(js)
    .pipe(concat('index.js'))
    .pipe(gulp.dest(paths.scripts.dest));

  // Archive
  gulp.src(paths.scripts.base + '/archive.js')
    .pipe(concat('archive.js'))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(livereload());
 
});
 
 
/**
 * Watch
 */
 
gulp.task('watch', function(){
 
  gulp.watch(paths.styles.base + '/skrollr.css', ['skrollr']);
  gulp.watch(paths.styles.base + '/*.scss', ['styles']);
  gulp.watch(paths.scripts.base + '/**/*.js', ['scripts']);
 
});
 
 
/**
 * Default
 */
 
gulp.task('default', function() {
 
  server = livereload();
  gulp.start('styles', 'scripts', 'watch');
 
});