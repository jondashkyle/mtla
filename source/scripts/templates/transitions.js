/**
 * Transitions
 */
Site.Transitions = Router({
  routes : {
    contact : {
      pattern : /^contact\/?$/,
      load : function(params){
        Site.Contact.load();
        Site.Navigation.setActive('contact');
      },
      unload : function() {
        Site.Contact.unload();
      }
    },
    press : {
      pattern : /^press(?:\/(.+))?$/,
      load : function(params){

        var path = params[0];
        Site.Navigation.setActive('press');

        // Tag + Project
        if ( path ) {
          if ( path.indexOf('tag:') > -1 ) {
            Site.Press.load();
          } else {
            Site.Publication.load();
          }
        } else {
          Site.Press.load();
        }

      },
      unload : function() {
        Site.Press.unload();
        Site.Publication.unload();
      }
    },
    studio : {
      pattern : /^studio(?:\/(.+))?$/,
      load : function(params){
        Site.Navigation.setActive('studio');
        Site.Studio.load();
      },
      unload : function(params) {
        Site.Studio.unload();
      }
    },
    project : {
      pattern : /^work(?:\/(.+))?$/,
      load : function(params) {

        var path = params[0];
        Site.Navigation.setActive('work');

        // Tag + Project
        if ( path ) {
          if ( path.indexOf('tag:') > -1 ) {
            Site.Work.load();
          } else {
            Site.Project.load();
          }
        } else {
          Site.Work.load();
        }

      },
      unload : function(params) {
        Site.Work.unload();
        Site.Project.unload();
      }
    },
    home : {
      pattern : /^\/?$/,
      load : function(params){
        Site.Home.load();
        Site.Navigation.setActive('/');
      },
      unload : function() {
        Site.Home.unload();
      }
    }
  },
  event : 'ready pjax:end',
  notfound : function() {
    // console.log('not found!');
    // do stuff on an unmatched route
  }
});