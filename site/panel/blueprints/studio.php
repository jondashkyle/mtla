<?php if(!defined('KIRBY')) exit ?>

title: Studio
pages:
  template: individual
files: false
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  medium
    buttons: true
  statement: 
    label: Statement
    type:  textarea
    size:  large
    buttons: true