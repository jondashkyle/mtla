<?php snippet('header') ?>

<div id="content">

  <div class="informationProject">
    Information
  </div>

  <div id="slideshowProject">
    <?php foreach( $page->images() as $image ): ?>
      <div class="slide">
        <img data-lazy="<?php echo thumb($image, array('width' => 1400 , 'height' => 1400), false); ?>" />
      </div>
    <?php endforeach ?>
  </div>

  <div id="contentProject">
    <div class="closeContent" data-contentClose>&times;</div>
    <div class="meta">
      <div class="projectTitle">
        <h2><?php echo html($page->title()) ?></h2>
      </div>
      <div class="projectMeta">
        <?php echo kirbytext($page->text()) ?>
        <?php if (strlen($page->partners()) > 1 ): ?>
          <h3>Partners</h3>
          <div class="partners">
            <?php echo kirbytext($page->partners()) ?>
          </div>
        <?php endif ?>
      </div>
    </div>
    <div class="projectNavigation">
      <?php
        $projectPrev = $page->prevVisible();
        $projectNext = $page->nextVisible();
      ?>
      <?php if ($projectPrev): ?>
      <div class="projectPrev">
        <a href="<?php echo $projectPrev->url() ?>" data-pjax><?php echo $projectPrev->title() ?></a>
      </div>
      <?php endif ?>
      <?php if ($projectNext): ?>
      <div class="projectNext">
        <a href="<?php echo $projectNext->url() ?>" data-pjax><?php echo $projectNext->title() ?></a>
      </div>
    <?php endif ?>
    </div>
  </div>

</div>

<?php snippet('footer') ?>