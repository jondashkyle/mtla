<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Press blocks
pages:
  template: publication
  limit: 30
files: true
fields:
  title: 
    label: Title
    type:  text