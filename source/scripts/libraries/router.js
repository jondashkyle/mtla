/**
 * Router
 */
var Router = function(options) {

  var data = {
    unload : null,
    routeName : null
  };
  
  var self = {
 
    options : $.extend(true, {}, {
      routes   : { },
      ignore   : null,
      event    : null,
      notFound : {
        load   : function() { },
        unload : function() { }
      }
    }, options),
 
    events : function() {
      if(self.options.event){
         $(document).on(self.options.event, function() {
          self.navigate();
        });
      }
    },

    add : function(opts) {
      if(('name' in opts) && ('pattern' in opts)){
        self.options.routes[opts.name] = {
          pattern : opts.pattern,
          load    : opts.load,
          unload  : opts.unload
        };
      } else {
        console.log('This route is broke');
      }
    },

    run : function(fcn, method, which, params) {
      fcn(params);
      $(window).trigger({
        type: 'route.' + method,
        route: which,
        info: params
      });
    },
 
    navigate : function() {
      var url = self.getCurrentRoute();
      var params;
      var load;
      var unload;
      var routeName;
      var lastRouteName;

      // Unload
      unload = data.unload ? data.unload : self.options.notFound.unload;
      lastRouteName = data.routeName ? data.routeName : null;
 
      Object.keys(self.options.routes).some(function(i) {
        params = self.matches(self.options.routes[i].pattern, url);
        if (params !== null) {
          load = self.options.routes[i].load;
          routeName = data.routeName = i;
          data.unload = self.options.routes[i].unload;
          return true;
        }
      });
 
      $(window).trigger({
        type: "route.change",
        url: url
      });
      
      // Update with the new route's load
      load = load ? load : self.options.notFound.load;

      self.run(unload, 'unload', lastRouteName, params);
      self.run(load, 'load', routeName, params);
    },
 
    matches : function(re, path) {
      var matches = re.exec(path);
      if (matches === null) return null;
      matches = matches.slice(1);
      matches = matches.map(function(val) {
        if (typeof val === 'undefined') return null;
        return val;
      });
      return matches;
    },
 
    getCurrentRoute : function() {
      if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
      }
      var origin    = window.location.origin;
      var path      = window.location.href;
      var pathArray = path.replace(origin,'').split( '/' );
      var builtPath = self.options.ignore ? pathArray.join("/").replace(self.options.ignore, '') : pathArray.join("/");
      if (builtPath.indexOf('/') === 0)
        builtPath = builtPath.replace('/','');
      return builtPath;
    }
 
  };
 
  // Initialize
  self.events();

  // Public methods
  return {
    data     : data,
    options  : self.options,
    add      : self.add,
    navigate : self.navigate
  }
 
};