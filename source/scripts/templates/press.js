/**
 * Press
 */
Site.Press = function() {

  var self = {

    resize : function() {
      
      var width    = $(window).width();
      var colWidth = 100 / Math.floor(width / 250);

      vein.inject('.pressBox', {
        'width' : colWidth + '%'
      });

    },

    load : function() {
      $(window).on('resize', self.resize).trigger('resize');
    },

    unload : function() {
      $(window).off('resize', self.resize);
    }

  };

  return {
    load   : self.load,
    unload : self.unload
  };

}();