/**
 * Intro
 */
Site.Intro = function() {

  var $this;

  var self = {

    $el : { },

    options : {
      'active' : true,
    },

    data : {

    },

    transitionConfig : function() {

      // Disable scrolling on body
      $('html').addClass('hideScroll');

      $('#logo').css('opacity', 0);

      // Position nav
      $('#navigation').velocity({
        'translateY' : '100%'
      }, {
        'duration' : 0,
      });

      // Latest work position
      setTimeout(function() {
        $('.slideshowHomeTitle')
          .css('z-index', 99)
          .velocity({
            'translateY' : $('.slideshowHomeTitle').outerHeight() * -1
          }, {
            'duration' : 0
          });
      }, 250);

    },

    transitionStart : function() {

      // Fade in the MTLA letters one by one
      setTimeout(function() {
        $('#introLogos .condensed g path').each(function(i) {
          $(this).velocity({
            'opacity' : 1
          }, {
            'easing' : 'easeInOutCubic',
            'duration' : 500,
            'delay'   : i * 100
          })
        });
      }, 250);

      // Slide in the letters of the expanded logo
      setTimeout(function() {
        $('#ARKMASK, #ANDSCAPEMASK, #RCHITECTUREMASK').each(function(i) {
          var width = parseInt($(this).attr('data-width'));
          $(this).velocity({
            'width' : [width, 0]
          }, {
            'delay'   : 500 * i,
            'duration' : 3000,
            'easing'  : 'easeOutCubic'
          });
        });
      }, 1000); 

      // Zoom in the img
      $this.velocity({
        'scale'  : 1.3,
        'translateZ' : 0,
      }, {
        'duration' : 0,
        complete : function() {
          $this.velocity({
          'scale' : 1.2,
          'opacity' : 1
          }, {
            'duration' : 1500,
            'delay'    : 800,
            'easing'   : 'easeInOutQuad',
            complete : function() {
              $('html, body').scrollTop(0);
              $('#introBackground').remove();
              self.bindScrollToStart();
            }
          });
        }
      });

    },

    bindScrollToStart : function() {
      $('#intro, #introLogos').on('click', self.scrollToStart);
      $(window).one('mousewheel', self.scrollToStart);
      setTimeout(function() {
        if ( self.options.active ) self.scrollToStart();
      }, 5 * 1000);
    },

    scrollToStart : function() {

      // Update data
      self.options.active = false;

      var winHeight = $(window).height();
      var winWidth  = $(window).width();
      var headerAttrs;

      // Show the nav and logo
      Site.Navigation.show();
      Site.Navigation.logoShow();

      $(window).off('mousemove', self.mousemove);

      // Hide the header
      $this.velocity({
        'scale' : 0.75,
        'translateZ' : 0,
        'backgroundPositionY' : '50%',
        'backgroundPositionX' : '50%'
      }, {
        'duration' : 1000,
        'easing'   : 'easeOutExpo',
        begin      : function() {
          $this.css({
            'position' : 'absolute',
          });
        },
        complete   : function() {
          $('html').removeClass('hideScroll');
          Site.Home.loadThumbnails();
          Site.Home.showDots();
          $this.velocity({
            'opacity' : 0
          }, {
            'duration' : 500,
            complete : function() {
              self.destroy();
               $('.slideshowHome').slickPlay();
            }
          });
        }
      });

      // Hide the intro logo
      $('#introLogos').velocity({
        'opacity' : 0,
      }, {
        'duration' : 500,
        'easing'   : 'easeInOutQuad',
        'display'  : 'none'
      });

      Site.Home.slideshowTitleShow();

    },

    scrollDisable : function(event) {

    },

    mousemove : function(event) {
      // var x = (event.clientX - $(window).width() / 2);
      // var y = (event.clientY - $(window).height() / 2);
      // console.log(x, y);
      // $('#intro').css('background-position', (x / 15) + '% ' + (y / 15) + '%');
    },

    resize : function() {

      var winHeight = $(window).height() - 65;
      var winWidth  = $(window).width();
      var scale, headerAttrs, logoScale;

      // Header/Intro aspect ratio
      if ( Site.Grid.full < winWidth ) {
        scale = Site.Grid.full / winWidth;
      } else {
        scale = winWidth / Site.Grid.full;
      }

      // Logo scale
      if ( winWidth < 1050 ) {
        logoScale = (winWidth - 100) / 1000;
      } else {
        logoScale = 1;
      }

      // Header/Intro positioning
      $this.velocity({
        'height' : winHeight,
        'width'  : winWidth
      }, {
        'duration' : 0
      });

      $('#introLogos').velocity({
        'scale' : logoScale
      }, 0);

    },

    /**
     * Bypass the intro by skipping the transition and hiding the
     * starting animation.
     */
    skip : function() {

      Site.Intro.options.active = false;

      self.destroy();

      // Show the nav and filter
      Site.Navigation.show();
      Site.Navigation.logoShow();
      Site.Home.slideshowTitleShow();
      Site.Home.loadThumbnails();
      if ( self.$el.slideshow ) Site.Home.showDots();

    },

    destroy : function() {

      // Remove elements from DOM
      $('#intro, #introBackground, #introLogos').remove();

      // Events
      $(window)
        .off('resize', self.resize)
        .off('mousemove', self.move);

    },

    setup : function() {

      // Cache
      $this = $('#intro');
      self.$el.logo    = $('#logo');
      self.$el.content = $('#content');

      // Events
      $(window)
        .on('resize', self.resize)
        .on('mousemove', self.mousemove)
        .trigger('resize');

      // Ready
      self.transitionConfig();
      Site.Loading.show();

      // Setup the image
      $('#intro').css({
        'background-image' : $('.slideshowHome .slide:first-child .img').css('background-image')
      })

      $('#intro').waitForImages({
        waitForAll: true,
        finished: function() {
          Site.Loading.hide();
          self.transitionStart();
        }
      });

    }

  };

  // Initialize
  $(function() {
    if ( $('#intro').length > 0 ) {
      self.setup();
    } else {
      self.skip();
    }
  });

  // Public
  return {
    bindStart : self.bindScrollToStart,
    options   : self.options
  }

}();