<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Updates
pages:
  template: update
  limit: 30
files: false
fields:
  title: 
    label: Title
    type:  text