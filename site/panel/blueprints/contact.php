<?php if(!defined('KIRBY')) exit ?>

title: Contact
pages: false
files: false
fields:
  title: 
    label: Title
    type:  text
  googlemaps: 
    label: Google Maps URL
    type:  text
  street: 
    label: Address PT1
    type:  text
  citystate: 
    label: Address PT2
    type:  text
  telephone: 
    label: Telephone Number
    type:  text
  email: 
    label: Email address
    type:  text
    validate: email