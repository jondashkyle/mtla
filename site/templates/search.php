<?php

  header('Content-type: application/json; charset=utf-8');  

  $data = $pages->find('work')->children()->visible()->flip();
  $json = array();

  foreach($data as $project) {

    // Search for the default but fallback
    if ( $project->images()->first() ) {
      $thumb = thumb($project->images()->first(), array('width' => 100 , 'height' => 100), false);
    } else {
      $thumb = '';
    }
    
    $json[] = array(
      'url'   => (string)$project->url(),
      'title' => (string)$project->title(),
      'tags'  => (string)$project->tags(),
      'city'  => (string)$project->city(),
      'state' => (string)$project->state(),
      'thumb' => (string)$thumb
    );
    
  }

  echo json_encode($json);

?>